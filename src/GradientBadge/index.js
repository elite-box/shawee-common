import styled from 'styled-components';

const GradientBadge = styled.span`
  padding: 8px 16px;
  border-radius: 1000px;
  font-weight: 600;
  color: #fff;
  background: linear-gradient(to right, #f00050, #ff7350);
`;

export default GradientBadge;
