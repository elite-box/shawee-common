import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Row, Icon } from 'antd';

import moment from 'moment';

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const Title = styled.h2`
  font-size: 2.1rem;
  margin-bottom: 0;
  word-break: break-word;

  @media screen and (max-width: 590px) {
    font-size: 1.8rem;
    & + .ant-row-flex {
      flex-direction: column;
      > a {
        margin-top: 16px;
      }
    }
  }
`;

const MetaRow = styled(Row)`
  @media screen and (max-width: 590px) {
    flex-direction: column;
    align-items: flex-start;
  }
`;

const Location = styled.div`
  display: flex;
  align-items: center;
  color: #9b9b9b;
  > span {
    margin: 0 4px;
    max-width: 150px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    display: inline-block;
  }
`;

const HackathonDate = Location.extend`
  margin-left: 16px;
  @media screen and (max-width: 590px) {
    margin-left: 0;
  }
`;

const HackathonInfo = ({ hackathon, children }) => (
  <Container>
    <Title>{hackathon.name}</Title>
    <Row type="flex" justify="space-between" align="middle">
      <MetaRow type="flex" justify="start" align="middle">
        <Location>
          <Icon type="environment-o" />
          <span title={hackathon.location}>{hackathon.location}</span>
        </Location>
        <HackathonDate>
          <Icon type="calendar" />
          <span>{moment(new Date(hackathon.startDate)).format('ll')}</span>
        </HackathonDate>
      </MetaRow>
      {children}
    </Row>
  </Container>
);

HackathonInfo.propTypes = {
  hackathon: PropTypes.objectOf(PropTypes.any).isRequired,
  children: PropTypes.node,
};

HackathonInfo.defaultProps = {
  children: null,
};

export default HackathonInfo;
