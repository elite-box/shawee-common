import styled from 'styled-components';

const Title = styled.h1`
  font-weight: 100;
  font-size: 2.5rem;
  margin-bottom: 0;
`;

export default Title;
