import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Input, Icon } from 'antd';

const Label = styled.label`
  strong {
    font-size: 16px;
    font-weight: 500;
  }
`;

export default class TextField extends Component {
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.defaultValue) {
      return {
        value: nextProps.defaultValue,
      };
    }
    return prevState;
  }

  constructor() {
    super();
    this.state = {
      currentIcon: null,
    };

    this.clearInput = this.clear.bind(this);

    this.icons = {
      success: <Icon type="check-circle-o" className="icon-success" />,
      error: <Icon type="close-circle-o" className="icon-error" />,
    };
  }


  clear() {
    this.setState({
      value: '',
      currentIcon: null,
    });
  }

  /**
   * Set the state with the input value and
   * validate the input with checkValidity funciton
   * @param {Event} e - Triggered event
   */
  handleInput(e) {
    const { value } = e.target;
    const { customValidation, handleInput } = this.props;
    if (handleInput) {
      handleInput(e);
    }

    if (customValidation) {
      e.target.setCustomValidity(customValidation(value));
    }

    this.checkValidity(e);
    this.setState({
      value,
    });
  }

  /**
   * Check if the input is valid and show an icon according the validation
   * @param {Event} e - Triggered event
   */
  checkValidity(e) {
    const { validity } = e.target;
    this.setState({
      currentIcon: validity.valid ? this.icons.success : this.icons.error,
    });
  }

  renderInputType = (id, currentIcon, value, validProps) => {
    switch (validProps.type) {
      case 'textarea':
        return (
          <Input.TextArea
            id={this.props.id}
            onBlur={e => this.checkValidity(e)}
            onInput={e => this.handleInput(e)}
            required
            suffix={currentIcon}
            value={value}
            {...validProps}
          />
        );
      case 'search':
        return (
          <Input.Search
            id={this.props.id}
            onBlur={e => this.checkValidity(e)}
            onInput={e => this.handleInput(e)}
            required
            suffix={currentIcon}
            value={value}
            {...validProps}
          />
        );
      default:
        return (
          <Input
            id={this.props.id}
            onBlur={e => this.checkValidity(e)}
            onInput={e => this.handleInput(e)}
            required
            suffix={currentIcon}
            value={value}
            {...validProps}
          />
        );
    }
  };

  render() {
    const { customClass, className, ...validProps } = this.props;

    const { wrapperStyle } = validProps;
    const { value, currentIcon } = this.state;

    delete validProps.wrapperStyle;
    delete validProps.handleInput;
    delete validProps.customValidation;
    return (
      <Label
        className={`${className} ${customClass}`}
        htmlFor={this.props.id}
        style={wrapperStyle}
      >
        <strong>{validProps.label}</strong>
        {this.renderInputType(this.props.id, currentIcon, value, validProps)}
      </Label>
    );
  }
}

TextField.propTypes = {
  customClass: PropTypes.string,
  customValidation: PropTypes.func,
  handleInput: PropTypes.func,
  id: PropTypes.string,
  defaultValue: PropTypes.string,
};

TextField.defaultProps = {
  customClass: null,
  customValidation: () => '',
  handleInput: null,
  id: null,
  defaultValue: null,
};
