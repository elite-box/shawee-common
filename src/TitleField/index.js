import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import TextField from '../TextField';

const Field = styled(TextField)`
  font-size: 30px;
  font-weight: bold;
  height: auto;
  background: none;
  border: none;
  border-bottom: 1px solid #d9d9d9;

  input {
    background: transparent;
    border: none;
    padding-left: 0;
    &:active, &:focus {
        box-shadow: none;
        border: none;
    }
  }
  
  &:focus {        
      outline: none;
      box-shadow: none;
  }
  ~ .ant-input-suffix {
      .anticon {
          font-size: 35px;
      }
  }
`;

export default class TitleField extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
  };

  static defaultProps = {
    className: '',
  };

  render() {
    return (
      <Field
        required
        handleInput={e => this.setState({ value: e.target.value })}
        {...this.props}
        className={`TitleField ${this.props.className}`}
      />
    );
  }
}
