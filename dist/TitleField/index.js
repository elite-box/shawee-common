'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _templateObject = _taggedTemplateLiteral(['\n  font-size: 30px;\n  font-weight: bold;\n  height: auto;\n  background: none;\n  border: none;\n  border-bottom: 1px solid #d9d9d9;\n\n  input {\n    background: transparent;\n    border: none;\n    padding-left: 0;\n    &:active, &:focus {\n        box-shadow: none;\n        border: none;\n    }\n  }\n  \n  &:focus {        \n      outline: none;\n      box-shadow: none;\n  }\n  ~ .ant-input-suffix {\n      .anticon {\n          font-size: 35px;\n      }\n  }\n'], ['\n  font-size: 30px;\n  font-weight: bold;\n  height: auto;\n  background: none;\n  border: none;\n  border-bottom: 1px solid #d9d9d9;\n\n  input {\n    background: transparent;\n    border: none;\n    padding-left: 0;\n    &:active, &:focus {\n        box-shadow: none;\n        border: none;\n    }\n  }\n  \n  &:focus {        \n      outline: none;\n      box-shadow: none;\n  }\n  ~ .ant-input-suffix {\n      .anticon {\n          font-size: 35px;\n      }\n  }\n']);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _TextField = require('../TextField');

var _TextField2 = _interopRequireDefault(_TextField);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Field = (0, _styledComponents2.default)(_TextField2.default)(_templateObject);

var TitleField = function (_PureComponent) {
  _inherits(TitleField, _PureComponent);

  function TitleField() {
    _classCallCheck(this, TitleField);

    return _possibleConstructorReturn(this, (TitleField.__proto__ || Object.getPrototypeOf(TitleField)).apply(this, arguments));
  }

  _createClass(TitleField, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(Field, _extends({
        required: true,
        handleInput: function handleInput(e) {
          return _this2.setState({ value: e.target.value });
        }
      }, this.props, {
        className: 'TitleField ' + this.props.className
      }));
    }
  }]);

  return TitleField;
}(_react.PureComponent);

TitleField.propTypes = {
  className: _propTypes2.default.string
};
TitleField.defaultProps = {
  className: ''
};
exports.default = TitleField;