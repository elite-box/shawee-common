'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _icon = require('antd/lib/icon');

var _icon2 = _interopRequireDefault(_icon);

var _input = require('antd/lib/input');

var _input2 = _interopRequireDefault(_input);

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _templateObject = _taggedTemplateLiteral(['\n  strong {\n    font-size: 16px;\n    font-weight: 500;\n  }\n'], ['\n  strong {\n    font-size: 16px;\n    font-weight: 500;\n  }\n']);

require('antd/lib/icon/style/css');

require('antd/lib/input/style/css');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Label = _styledComponents2.default.label(_templateObject);

var TextField = function (_Component) {
  _inherits(TextField, _Component);

  _createClass(TextField, null, [{
    key: 'getDerivedStateFromProps',
    value: function getDerivedStateFromProps(nextProps, prevState) {
      if (nextProps.defaultValue) {
        return {
          value: nextProps.defaultValue
        };
      }
      return prevState;
    }
  }]);

  function TextField() {
    _classCallCheck(this, TextField);

    var _this = _possibleConstructorReturn(this, (TextField.__proto__ || Object.getPrototypeOf(TextField)).call(this));

    _this.renderInputType = function (id, currentIcon, value, validProps) {
      switch (validProps.type) {
        case 'textarea':
          return _react2.default.createElement(_input2.default.TextArea, _extends({
            id: _this.props.id,
            onBlur: function onBlur(e) {
              return _this.checkValidity(e);
            },
            onInput: function onInput(e) {
              return _this.handleInput(e);
            },
            required: true,
            suffix: currentIcon,
            value: value
          }, validProps));
        case 'search':
          return _react2.default.createElement(_input2.default.Search, _extends({
            id: _this.props.id,
            onBlur: function onBlur(e) {
              return _this.checkValidity(e);
            },
            onInput: function onInput(e) {
              return _this.handleInput(e);
            },
            required: true,
            suffix: currentIcon,
            value: value
          }, validProps));
        default:
          return _react2.default.createElement(_input2.default, _extends({
            id: _this.props.id,
            onBlur: function onBlur(e) {
              return _this.checkValidity(e);
            },
            onInput: function onInput(e) {
              return _this.handleInput(e);
            },
            required: true,
            suffix: currentIcon,
            value: value
          }, validProps));
      }
    };

    _this.state = {
      currentIcon: null
    };

    _this.clearInput = _this.clear.bind(_this);

    _this.icons = {
      success: _react2.default.createElement(_icon2.default, { type: 'check-circle-o', className: 'icon-success' }),
      error: _react2.default.createElement(_icon2.default, { type: 'close-circle-o', className: 'icon-error' })
    };
    return _this;
  }

  _createClass(TextField, [{
    key: 'clear',
    value: function clear() {
      this.setState({
        value: '',
        currentIcon: null
      });
    }

    /**
     * Set the state with the input value and
     * validate the input with checkValidity funciton
     * @param {Event} e - Triggered event
     */

  }, {
    key: 'handleInput',
    value: function handleInput(e) {
      var value = e.target.value;
      var _props = this.props,
          customValidation = _props.customValidation,
          handleInput = _props.handleInput;

      if (handleInput) {
        handleInput(e);
      }

      if (customValidation) {
        e.target.setCustomValidity(customValidation(value));
      }

      this.checkValidity(e);
      this.setState({
        value: value
      });
    }

    /**
     * Check if the input is valid and show an icon according the validation
     * @param {Event} e - Triggered event
     */

  }, {
    key: 'checkValidity',
    value: function checkValidity(e) {
      var validity = e.target.validity;

      this.setState({
        currentIcon: validity.valid ? this.icons.success : this.icons.error
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          customClass = _props2.customClass,
          className = _props2.className,
          validProps = _objectWithoutProperties(_props2, ['customClass', 'className']);

      var wrapperStyle = validProps.wrapperStyle;
      var _state = this.state,
          value = _state.value,
          currentIcon = _state.currentIcon;


      delete validProps.wrapperStyle;
      delete validProps.handleInput;
      delete validProps.customValidation;
      return _react2.default.createElement(
        Label,
        {
          className: className + ' ' + customClass,
          htmlFor: this.props.id,
          style: wrapperStyle
        },
        _react2.default.createElement(
          'strong',
          null,
          validProps.label
        ),
        this.renderInputType(this.props.id, currentIcon, value, validProps)
      );
    }
  }]);

  return TextField;
}(_react.Component);

exports.default = TextField;


TextField.propTypes = {
  customClass: _propTypes2.default.string,
  customValidation: _propTypes2.default.func,
  handleInput: _propTypes2.default.func,
  id: _propTypes2.default.string,
  defaultValue: _propTypes2.default.string
};

TextField.defaultProps = {
  customClass: null,
  customValidation: function customValidation() {
    return '';
  },
  handleInput: null,
  id: null,
  defaultValue: null
};