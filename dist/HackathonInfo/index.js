'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _icon = require('antd/lib/icon');

var _icon2 = _interopRequireDefault(_icon);

var _row = require('antd/lib/row');

var _row2 = _interopRequireDefault(_row);

var _templateObject = _taggedTemplateLiteral(['\n  display: flex;\n  flex-direction: column;\n'], ['\n  display: flex;\n  flex-direction: column;\n']),
    _templateObject2 = _taggedTemplateLiteral(['\n  font-size: 2.1rem;\n  margin-bottom: 0;\n  word-break: break-word;\n\n  @media screen and (max-width: 590px) {\n    font-size: 1.8rem;\n    & + .ant-row-flex {\n      flex-direction: column;\n      > a {\n        margin-top: 16px;\n      }\n    }\n  }\n'], ['\n  font-size: 2.1rem;\n  margin-bottom: 0;\n  word-break: break-word;\n\n  @media screen and (max-width: 590px) {\n    font-size: 1.8rem;\n    & + .ant-row-flex {\n      flex-direction: column;\n      > a {\n        margin-top: 16px;\n      }\n    }\n  }\n']),
    _templateObject3 = _taggedTemplateLiteral(['\n  @media screen and (max-width: 590px) {\n    flex-direction: column;\n    align-items: flex-start;\n  }\n'], ['\n  @media screen and (max-width: 590px) {\n    flex-direction: column;\n    align-items: flex-start;\n  }\n']),
    _templateObject4 = _taggedTemplateLiteral(['\n  display: flex;\n  align-items: center;\n  color: #9b9b9b;\n  > span {\n    margin: 0 4px;\n    max-width: 150px;\n    overflow: hidden;\n    white-space: nowrap;\n    text-overflow: ellipsis;\n    display: inline-block;\n  }\n'], ['\n  display: flex;\n  align-items: center;\n  color: #9b9b9b;\n  > span {\n    margin: 0 4px;\n    max-width: 150px;\n    overflow: hidden;\n    white-space: nowrap;\n    text-overflow: ellipsis;\n    display: inline-block;\n  }\n']),
    _templateObject5 = _taggedTemplateLiteral(['\n  margin-left: 16px;\n  @media screen and (max-width: 590px) {\n    margin-left: 0;\n  }\n'], ['\n  margin-left: 16px;\n  @media screen and (max-width: 590px) {\n    margin-left: 0;\n  }\n']);

require('antd/lib/icon/style/css');

require('antd/lib/row/style/css');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents2.default.div(_templateObject);

var Title = _styledComponents2.default.h2(_templateObject2);

var MetaRow = (0, _styledComponents2.default)(_row2.default)(_templateObject3);

var Location = _styledComponents2.default.div(_templateObject4);

var HackathonDate = Location.extend(_templateObject5);

var HackathonInfo = function HackathonInfo(_ref) {
  var hackathon = _ref.hackathon,
      children = _ref.children;
  return _react2.default.createElement(
    Container,
    null,
    _react2.default.createElement(
      Title,
      null,
      hackathon.name
    ),
    _react2.default.createElement(
      _row2.default,
      { type: 'flex', justify: 'space-between', align: 'middle' },
      _react2.default.createElement(
        MetaRow,
        { type: 'flex', justify: 'start', align: 'middle' },
        _react2.default.createElement(
          Location,
          null,
          _react2.default.createElement(_icon2.default, { type: 'environment-o' }),
          _react2.default.createElement(
            'span',
            { title: hackathon.location },
            hackathon.location
          )
        ),
        _react2.default.createElement(
          HackathonDate,
          null,
          _react2.default.createElement(_icon2.default, { type: 'calendar' }),
          _react2.default.createElement(
            'span',
            null,
            (0, _moment2.default)(new Date(hackathon.startDate)).format('ll')
          )
        )
      ),
      children
    )
  );
};

HackathonInfo.propTypes = {
  hackathon: _propTypes2.default.objectOf(_propTypes2.default.any).isRequired,
  children: _propTypes2.default.node
};

HackathonInfo.defaultProps = {
  children: null
};

exports.default = HackathonInfo;