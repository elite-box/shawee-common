'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _templateObject = _taggedTemplateLiteral(['\n  padding: 8px 16px;\n  border-radius: 1000px;\n  font-weight: 600;\n  color: #fff;\n  background: linear-gradient(to right, #f00050, #ff7350);\n'], ['\n  padding: 8px 16px;\n  border-radius: 1000px;\n  font-weight: 600;\n  color: #fff;\n  background: linear-gradient(to right, #f00050, #ff7350);\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var GradientBadge = _styledComponents2.default.span(_templateObject);

exports.default = GradientBadge;